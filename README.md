SimplyBiz

Desing to buil infrastructure for new deployment in SimplyBiz

**Building infrastructure**

1.- Create a template and desing using terraform for building resources, then connect them using console (VPN)
2.- You need to connect to the Jump Host, deploy from there to the GitLab repository to the AWS console.


**Production infrastructure**

1- Create a VPC and subnets to mantain this infrastructure isolated. One Public subnet and one private subnet.

2.- you build security group to connect to the front-end using elastic load balancing. This Security group will be using just ports 443 and 22 to connecto the instances.

3.- When the users connecto to the front-end they will reach auto-scaling group to request information, auto-scaling was configured to start with 2 instances running with load workload as default, second action is mantain 4 instances to manage more than 50 % of workload, if the auto-scaling need to manage more request, it will be able to grown until 8 instances, this is enough to meet the requirements.

4.- Users need to reach on-premises clustered TIER application using VPN connect to perform actions, VPN works to connecto on-premises servers and databases with application TIER, users will reach one NGNIX high avalability solution to perform request to the application TIER clustered solution, TIER application will be consumming on data from Clustered Databases as data source.

**Deployment infrastructure**

Deploymente infrastructure is the same like production, most important changes are.

1.- You need to push your code to your GitLab repository
2.- GitLab send updates to deployment infrastructure to test your new code using CI/CD pipeline.
3.- When code was tested and approve by QA team, you can deploy code you need into production environment.
4.- Do new push to environment production repository to deploy using CI/CD pipeline to perform actions on productions environment.

**Support**

You can use Jump Host hosted in public subnet to connect to whole environment to support them.

